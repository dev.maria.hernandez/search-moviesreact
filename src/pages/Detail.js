import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { ButtonBacktoHome } from '../components/ButtonBacktoHome';

const API_KEY = 'b0562050&s'

export class Detail extends Component{
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.object,
            isEcaxt: PropTypes.bool,
            path: PropTypes.string,
            url: PropTypes.string
        })
    }
    state = {
        movie: {}
    }

    _fetchMovie ({id})  {
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
            .then(res => res.json())
            .then(movie => {
                console.log(movie)
                this.setState({movie})
            })
    }


    componentDidMount(){
        const {id} = this.props.match.params
        this._fetchMovie({id})
    }

    render(){
        const { Title, Poster, Actors, Mestascore, Plot} = this.state.movie
        return(
            <div>
                <ButtonBacktoHome />
                <h1>{Title}</h1>
                <img src={Poster} />
                <h3>{Actors}</h3>
                <span>{Mestascore}</span>
                <p>{Plot}</p>
            </div>
        )
    }
}